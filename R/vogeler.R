## =====================================
## vogeler
## copyright Frédéric Vanwindekens, 2020
## =====================================

## (setq ess-roxy-template-alist (list (cons "description" ".. content for ==description== (no empty lines) ..")
##                                     (cons "details" ".. content for ==details== ..")
##                                     (cons "title" "Your title")
##                                     (cons "param" "")
##                                     (cons "return" "Something")
##                                     (cons "export" "")))

#' Create a standard set of folders for a Vogeler project
#'
#' @param project_name  A string project name to be located in the
#'                      current working directory or a path to a project folder.
#' @param sample Logical that indicates that the sample data should be copied to
#'     the project.
#' @examples
#' create_vogeler_project(project_name = "my_vogeler_project")
#' unlink("./my_vogeler_project", recursive=TRUE)
#' @export
create_vogeler_project<- function(project_name, sample = FALSE){
  if(!dir.exists(project_name)){
  dir.create(project_name)
  }else{
    message(paste0("dir:", project_name, "already exists"))
  }
  for (folder in c("data-raw","fig","data-output")) {
      if(!dir.exists(file.path(project_name, folder))){
          dir.create(file.path(project_name, folder))
      }else{
          message(paste0("dir:", project_name,folder, "already exists"))
      }
  }
  
  if (sample){
      file.copy(from = system.file("data-raw",  package = "vogeler"),
                project_name, recursive = TRUE )
  }

  invisible(TRUE)
}

##' .. content for ==description== (no empty lines) ..
##'
##' .. content for ==details== ..
##' @title Your title
##' @param db 
##' @return Something
##' @export 
loadSlakes <- function(db){
    output = list(value = NULL, condition = list(type = NULL, message = NULL))
    snitch = FALSE
    do.loadSlakes <- function(){
        failed.slakes <- loadData("3-slakes-fail.csv")
        slakes <- loadData(paste0("3-slakes-",db,".csv")) %>%
            dplyr::filter(!(slake %in% failed.slakes$slake))
        return(slakes)
    }

    tryCatch(do.loadSlakes(),
             error = function(cond){
                 err.msg = paste0(cond)
                 message(err.msg)
                 return("bofbof")},
             finally = message(paste0("Table of slakes from database :", db)))
}

create.random.obs <- function(n.obs=10, file){
    for (i in 1:n.obs) {
        bird <- sample(birds$nom,1)
        place <- sample(places$Lieu,1)
        timestamp <- format(as.POSIXct(
            sample(1:640000000,1),
            origin = "2000-01-01", tz = "UTC"), "%Y-%m-%d %H:%M"
            )
        
        obs.data <- list(
            place = place,
            bird = bird,
            timestamp = timestamp,
            notes = "some notes about this vogel")
        
        write.table(
            x = obs.data,
            file = file, 
            col.names = FALSE, row.names = FALSE,
            quote = TRUE, append = TRUE, sep = ","
        )
    }
}

