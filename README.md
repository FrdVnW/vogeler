
# Table of Contents

1.  [Introduction](#org61676a4)
2.  [Installation](#orgc03cc69)
    1.  [Installation of the package from gitlab in R](#org15dc4c2)
    2.  [Loading of the package from your computer, without installation](#org6c9ce7f)
3.  [Usage](#orgaab25ea)
    1.  [Load the package](#org833c195)
    2.  [Usage of the slaker package](#org46ad5ae)
        1.  [Create the structure](#org0d88707)
        2.  [Look at the structure and mimic it for your own project](#org0c8e7fe)
        3.  [Set working directory (in your project) and define configuration variable(s)](#org099f990)
        4.  [Run the application](#org2576d9f)



<a id="org61676a4"></a>

# Introduction

This R package is in an early development stage. It provides functions and application for organising and visualisation your own bird observations and census.


<a id="orgc03cc69"></a>

# Installation

vogeler is on development and hosted on Gitlab. 
Using the package `devtools` <sup><a id="fnr.1" class="footref" href="#fn.1">1</a></sup> to install `vogeler`, you can install it with the command : 

    install.packages("devtools")


<a id="org15dc4c2"></a>

## Installation of the package from gitlab in R

    devtools::install_git("https://gitlab.com/FrdVnW/vogeler", upgrade_dependencies = FALSE)
    ## install_gitlab("FrdVnW/slaker", upgrade = "never") ## new version of devtools


<a id="org6c9ce7f"></a>

## Loading of the package from your computer, without installation

If `vogeler` is already on your computer, you can use this command to load it : 

    devtools::load_all("/PATH/TO/PACKAGE/vogeler")


<a id="orgaab25ea"></a>

# Usage


<a id="org833c195"></a>

## Load the package

    library(vogeler)


<a id="org46ad5ae"></a>

## Usage of the slaker package


<a id="org0d88707"></a>

### Create the structure

Here with examples of data files and 10.000 random observation between 2000-2020

Open R or RStudio in a well defined working directory on your computer. And create the project structure with the function `create_slaker_project`.

    vogeler::create_vogeler_project("projet_XYZ", sample=TRUE)

Here, example files are given in data-raw sub-folder of the project.


<a id="org0c8e7fe"></a>

### Look at the structure and mimic it for your own project


<a id="org099f990"></a>

### Set working directory (in your project) and define configuration variable(s)

    setwd("./projet_XYZ/")
    
    project_path <- paste0(getwd(),"/")


<a id="org2576d9f"></a>

### Run the application

    vogeler()

**WORK IN PROGRESS**


# Footnotes

<sup><a id="fn.1" href="#fnr.1">1</a></sup> if needed `install.packages(devtools)`
