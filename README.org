# -*- eval: (setq process-name "*R:vogeler*") ; eval: (load "dev-R.el") ; eval: (local-set-key "\C-c\C-q" 'ess-quit) -*-
# Copyright (C) 2020 Fred Vanwin

#+STARTUP: showall indent
#+STARTUP: hidestars
#+TITLE:vogeler
#+SUBTITLE: Bird census, visualisation and game
#+AUTHOR: Fred Vanwin
#+email: FrdVnW@pm.me
# +OPTIONS: toc:nil num:nil
# -----

#+LaTeX_CLASS: koma-article
#+LaTeX_HEADER: \usepackage[round, sort]{natbib}
#+LaTeX_HEADER: \usepackage{bookman}

#+PROPERTY: header-args:R :exports code :session *R* :tangle yes :eval never-export

# README.md is generated from README.org. Please edit the .org file

* Introduction

This R package is in an early development stage. It provides functions and application for organising and visualisation your own bird observations and census.

* Installation

  vogeler is on development and hosted on Gitlab. 
  Using the package =devtools= [fn::if needed =install.packages(devtools)=] to install =vogeler=, you can install it with the command : 

  #+BEGIN_SRC R
    install.packages("devtools")
  #+END_SRC

** Installation of the package from gitlab in R
  #+BEGIN_SRC R
  devtools::install_git("https://gitlab.com/FrdVnW/vogeler", upgrade_dependencies = FALSE)
  ## install_gitlab("FrdVnW/slaker", upgrade = "never") ## new version of devtools
  #+END_SRC
** Loading of the package from your computer, without installation

If =vogeler= is already on your computer, you can use this command to load it : 

#+BEGIN_SRC R :results silent
devtools::load_all("/PATH/TO/PACKAGE/vogeler")
#+END_SRC


* Usage
** Load the package
#+BEGIN_SRC R :results silent
library(vogeler)
#+END_SRC


** Usage of the slaker package
*** Create the structure 

Here with examples of data files and 10.000 random observation between 2000-2020

Open R or RStudio in a well defined working directory on your computer. And create the project structure with the function =create_slaker_project=.

#+BEGIN_SRC R 
  vogeler::create_vogeler_project("projet_XYZ", sample=TRUE)
#+END_SRC

Here, example files are given in data-raw sub-folder of the project.

*** Look at the structure and mimic it for your own project

*** Set working directory (in your project) and define configuration variable(s)
#+BEGIN_SRC R
setwd("./projet_XYZ/")

project_path <- paste0(getwd(),"/")
#+END_SRC


*** Run the application 
#+BEGIN_SRC R 
  vogeler()
#+END_SRC


*WORK IN PROGRESS*


