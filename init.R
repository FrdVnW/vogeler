## ========================

## Frédéric M. Vanwindekens

    ##    O
    ##  /||\
    ##   \\           
    ##  / /
    ## °°°°

## ========================

## THIS SCRIPT is a program implementing ...
## Copyright (C) 2020 Frédéric Vanwindekens


source("./R/vogeler.R")

## Data Creation
## birds <- read.csv("./inst/data-raw/a_birds_list.csv")
## places <- read.csv("./inst/data-raw/b_places.csv")
## create.random.obs(10000,"./inst/data-raw/c_birds_observations_random.csv")

library(shiny)
library(ggplot2)
library(dplyr)
library(lubridate)
options(browser="firefox")


project_path <- paste0(getwd(),"/inst/")

runApp('./inst/shiny/vogeler/')

## Scratch
data.obs <- read.csv("./inst/data-raw/c_birds_observations_random.csv")

data.obs$time <- as.POSIXct(data.obs$timestamp,format="%Y-%m-%d %H:%M")
head(data.obs)

graph <- (ggplot(data=data.obs, aes(x = time, y = bird))
    + geom_jitter(aes(colour = place),height = 0.1)
    )

print(graph)


data.monthly <- data.obs %>%
    group_by(bird, place, month = floor_date(time, "month")) %>%
    count(bird)

head(data.monthly)


graph <- (ggplot(data=data.monthly, aes(x = month, y = n))
    + geom_line(aes(colour = place))
    + facet_wrap(bird ~ .)
    )

print(graph)

