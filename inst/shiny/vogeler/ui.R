ui <- fluidPage(
    title = "vogeleR",
    titlePanel(fluidRow(column(width = 1, tags$img(src = "whirlwheels.png", width = "50px",height = "50px")),
                        column(width = 11,  "Vogeler - vogels census", align='center')
                        )
               ),
    fluidRow(column(width = 6, textInput('user', "Prénom : "))),
    navbarPage(
        "Whirl! biology",         
        ## tags$img(src = "", width = "50px",height = "50px"),
        ## header = ,
        footer = list(tags$br(), tags$br(),
                      tags$img(src = "whirlwheels.png", width = "200px",height = "200px"),
                      tags$br(), tags$br(),
                      tags$strong("Whirl! éducation -- Copyright (C) 2020 Frédéric Vanwindekens, Salzinnes (Belgium)")),
        tabPanel("Vogels", fluid = TRUE,
                 tabsetPanel(
                     tabPanel("Vogels list", fluid = TRUE,
                              sidebarLayout(
                                  sidebarPanel(
                                      htmlOutput('salut'),
                                      h3("Vogels Data file"),
                                      uiOutput('select.files.birds'),
                                      
                                      tags$hr(),
                                      
                                      h3("Nouveau spécimen"),

                                      textInput("new.nom", "Nom (Fr)", ""),
                                      textInput("new.nom_latin", "Nom latin", ""),
                                      textInput("new.naam", "Naam (Ndls)", ""),
                                      textInput("new.name", "Name (Eng.)", ""),
                                      actionButton("submit.specimen", "Ajouter spécimen")
                                  ),
                                  mainPanel(
                                      h2("Your vogels list"),
                                      tableOutput("t.list.birds")
                                  )
                              )
                              ),
                     tabPanel("Places list", fluid = TRUE,
                              sidebarLayout(
                                  sidebarPanel(
                                      h3("Places Data file"),
                                      uiOutput('select.files.places'),
                                      
                                      tags$hr(),
                                      
                                      h3("Nouveau point de vue"),

                                      textInput("new.lieu", "Lieu", ""),
                                      textInput("new.commune", "Commune", ""),
                                      textInput("new.pays", "Pays", ""),
                                      textInput("new.lat", "Lat.", ""),
                                      textInput("new.lon", "Lon.", ""),
                                      actionButton("submit.place", "Ajouter point de vue")

                                      ## h3("New campaign, new serie"),

                                      ## textInput("campaign", "Campagne", ""),
                                      ## textInput("serie", "Essai", ""),
                                      ## textInput("crop", "Culture", ""),
                                      ## textInput("parcel", "Parcelle", ""),
                                      ## textInput("bloc", "Bloc", ""),
                                      ## textInput("modalite", "Modalité", ""),
                                      ## actionButton("submitSerie", "Submit")
                                  ),
                                  mainPanel(
                                      h2("Your places list"),
                                      tableOutput("t.list.places")
                                  )
                              )
                              ),
                     tabPanel("Observation form", fluid = TRUE,
                              sidebarLayout(
                                  sidebarPanel(
                                      h3("Encodage des observations"),
                                      uiOutput('select.files.observations'),
                                      uiOutput('select.bird'),
                                      uiOutput('select.place'),
                                      textInput("notes","Notes"),
                                      radioButtons("obs.mode",
                                                   "Mode d'observation",
                                                   choices=c("Encodage a posteriori"="post",
                                                             "Encodage en direct"="live"),
                                                   selected="post",inline=TRUE),
                                      conditionalPanel(
                                          condition = "(input['obs.mode'] == 'live')",
                                          helpText('Observation en direct')
                                      ),
                                      conditionalPanel(
                                          condition = "(input['obs.mode'] == 'post')",
                                          helpText('Encodage a posteriori'),
                                          fluidRow(
                                              column(width = 6,
                                                     dateInput("obs.date", "Date :", value = Sys.Date())
                                                     ),
                                              column(width = 6, 
                                                     sliderInput("obs.time", "Time :", 0, 23, 12, 1)
                                                     )
                                          )
                                      ),
                                      uiOutput('obs.sent'),
                                      actionButton("update", "Mise à jour des observations", icon = icon("sync")),
                                      tags$hr()
                                      
                                      ## h3("New campaign, new serie"),

                                      ## textInput("campaign", "Campagne", ""),
                                      ## textInput("serie", "Essai", ""),
                                      ## textInput("crop", "Culture", ""),
                                      ## textInput("parcel", "Parcelle", ""),
                                      ## textInput("bloc", "Bloc", ""),
                                      ## textInput("modalite", "Modalité", ""),
                                      ## actionButton("submitSerie", "Submit")
                                  ),
                                  mainPanel(
                                      h2("Your observations"),
                                      tableOutput("t.list.observations")
                                  )
                              )
                              ),
                     tabPanel("Analyser vos observations", fluid = TRUE,
                              sidebarLayout(
                                  sidebarPanel(
                                      h3("Places Data file"),
                                      uiOutput('select.files.observations.analyses'),
                                      tags$hr()
                                  ),
                                  mainPanel(
                                      h2("Fréquence")
                                  )
                              )
                              )                     
                 )
                 )
    )
)


##         tabPanel("New samples", fluid = TRUE,
##                  sidebarLayout(
##                      sidebarPanel(
##                          h3("Fichier des parcelles"),
##                          uiOutput("v.files.series.samples"),
##                          h3("Fichier des échantillons"),
##                          uiOutput("v.files.samples"),
                         
##                          tags$hr(),
                         
##                          h3("New sample"),

##                          uiOutput("v.campaigns.samples"),
##                          uiOutput("v.serie.samples"),
##                          textInput("sample", "Sample", ""),
##                          textInput("mode", "Mode de prélèvement", ""),
##                          radioButtons("drying_temp",
##                                       "Drying method (temp)",
##                                       choices=c("Air libre","50 °C","105°C"),
##                                       selected="50 °C"),
##                          sliderInput("drying_j",
##                                      "Drying method (days)",1,30,2),
##                          radioButtons("eau",
##                                       "Water type",
##                                       choices=c("Eau déminéralisée"),
##                                       selected="Eau déminéralisée"),
##                          actionButton("submitSample", "Submit")
##                      ),
##                      mainPanel(
##                          tableOutput("samples"), tags$hr()
##                      )
##                  )
##                  ),
##         tabPanel("New slake test", fluid = TRUE,
##                  sidebarLayout(
##                      sidebarPanel(
##                          h3("Data base and data source"),
##                          selectInput("operator",
##                                      "Operator : ",
##                                      c("François Pignon",
##                                        "Vincent Gausset",
##                                        "Cyril André",
##                                        "Fred Vanwin",
##                                        "Brieuc Hardy",
##                                        "Christian Roisin",
##                                        "Bruno Huyghebaert",
##                                        "Etudiant 1",
##                                        "Etudiant 2",
##                                        "Mathieu Dufey")),
##                          uiOutput("v.ports.slakes"),
##                          uiOutput("v.files.series.slakes"),
##                          uiOutput("v.files.samples.slakes"),
                         
##                          h3("New slake test"),
##                          uiOutput("v.campaigns.slake"),
##                          uiOutput("v.serie.slake"),
##                          uiOutput("v.sample.slake"),
##                          ## textInput("slake", "Slake nr", "000888"),
                         
##                          actionButton("startslake", "Start the Slake Test",width='100%',icon=icon("play-circle")),
##                          actionButton("stopslake", "Stop the Slake Test",width='100%',icon=icon("stop-circle")),

##                          h3("Some options"),
##                          sliderInput("maxTime",
##                                      label="Maximum time [min]",
##                                      min=10,
##                                      max=1200,
##                                      value=120),
##                          sliderInput("phaseIITime",
##                                      label="Start Phase II (1 record/10 sec) [min]",
##                                      min=3,
##                                      max=10,
##                                      value=3),
##                          sliderInput("phaseIIITime",
##                                      label="Start Phase III (1 record/30 sec) [min]",
##                                      min=10,
##                                      max=30,
##                                      value=10)
##                          ## actionButton("update", "Update view",icon = icon("refresh"),width='100%')
##                      ),
##                      mainPanel(
##                          htmlOutput("titleslake"),
##                          verbatimTextOutput("slake.vals"),
##                          h3("Iteration: "),
##                          verbatimTextOutput("slake.i"),
##                          h3("Mass:"),
##                          verbatimTextOutput("slake.mass"),
##                          ## "Time:", ## trim ???
##                          ## textOutput("slake.time"),
##                          h3("Elapsed Time (seconds):"),
##                          verbatimTextOutput("elapsed"),
##                          hr(),
##                          h3("A simple slake visualisation"),
##                          plotOutput("slake.vis.lite")
##                      )
##                  )
##                  ),
##         tabPanel("Slake visualizer",fluid = TRUE,
##                  sidebarLayout(
##                      sidebarPanel(                                                
##                          actionButton("update", "Update view", icon = icon("refresh"),width='100%'),
                         
##                          h3("Main data source"),
##                          uiOutput("v.files.series.visualizer"),
##                          uiOutput("v.files.samples.visualizer"),
##                          uiOutput("v.files.slakes.visualizer"),
                         
##                          helpText("A - Slake tests filtering"),
##                          uiOutput("v.campaigns.visualizer"),
##                          uiOutput("v.drying.visualizer"),
##                          uiOutput("v.modalite.visualizer"),
##                          ## uiOutput("v.culture.visualizer"),
##                          uiOutput("v.bloc.visualizer"),
##                          uiOutput("v.parcelle.visualizer"),

##                          helpText("B - Plot design"),
##                          uiOutput("v.num.slakes"),
##                          ## conditionalPanel(condition="input.tabs == 'plotindic'",
##                          ##                  uiOutput("v.parcelle.visualizer.indic")),
##                          uiOutput("v.criteria"),
##                          sliderInput(inputId = "timemax.viz",
##                                      label = "Limit of the time of the plot (minutes)",
##                                      min = 1,
##                                      max = 120,
##                                      value = 20),
##                          radioButtons(inputId = "timecomp.viz",
##                                       label = "The time for the statistical comparison (minutes)",
##                                       choices = c(0,0.5,1,2,5,10,11,12,13,14,15,20,30,40),
##                                       selected = 13,
##                                       inline = TRUE ),
##                          radioButtons(inputId = "bloc.stat",
##                                       label = "Blocking ?",
##                                       choices = c(FALSE,TRUE),
##                                       selected = FALSE,
##                                       inline = TRUE ),
##                          width = 3
##                      ),
                     
##                      mainPanel(
##                          tabsetPanel(
##                              tabPanel("Plot", value="plot",
##                                       plotOutput("slakeplotcomp")
##                                       ),
##                              tabPanel("Plot indicators", value="plotindic",
##                                       uiOutput("v.parcelle.visualizer.indic"),
##                                       plotOutput("slakeplotindic")
##                                       ),
##                              tabPanel("Table indicators",
##                                       tableOutput("slaketableindic")
##                                       ),
##                              tabPanel("Statistics",
##                                       h3(textOutput("titleplotstat")),
##                                       h4("N.B. - dots are mean, bars are standard deviation"),
##                                       plotOutput("slakestatcompplot"),
##                                       h3(textOutput("titleanovostat")),
##                                       verbatimTextOutput("slakestatcompaov")
##                                       ),
##                              tabPanel("Data",
##                                       tableOutput("slake.serie.samples.viz")
##                                       ),
##                              tabPanel("Help (legend)",
##                                       h3("Acronyms of indicators"),
##                                       p("Wmax = Weight max after immersion (after bubbles)"),
##                                       p("All weights are relative to Wmax"),
##                                       p("Wd = Weight dry soil"),
##                                       p("Wmax/Wd = Warchi = Archimède ? Density ?"),
##                                       p("Wt0 = Weight dry soil"),
##                                       p("T0 = Time at immersion"),
##                                       p("Tmax = Time at Wmax"),
##                                       p("Wt0 = Weight at T0"),
##                                       p("Wmax - Wt0 = 1 - Wt0"),
##                                       p("Pente T0-Max = Sl.0.max = pente entre T0 et Max"),
##                                       p("Pente Max-30 = Sl.max.30 = pente entre Max et Max+30(sec.)"),
##                                       p("Pente Max-60 = Sl.max.60 = pente entre Max et Max+60(sec.)")
##                                       )
##                          ),
##                          width = 9
##                      )
##                  )
##                  )
##     )
## )

## Scratch

## fileInput("file.series", "Choose CSV File",
##           multiple = FALSE,
##           accept = c(
##               "text/csv",
##               "text/comma-separated-values,text/plain",
##               ".csv")
##           ),

## actionButton("updatetable", "Update table",icon = icon("refresh"),width='100%'),
